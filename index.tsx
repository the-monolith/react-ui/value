import { component, React, Style } from 'local/react/component'

export const defaultValueStyle: Style = {
  display: 'inline',
  appearance: 'none',
  border: 'none',
  fontFamily: 'inherit',
  fontSize: 'inherit',
  color: 'inherit',
  backgroundColor: 'transparent',
  flexShrink: 0,
  minWidth: '100%',
  padding: 0,
  margin: 0
}

export const Value = component.children
  .props<{ value: string; className?: string; style?: Style }>({
    style: defaultValueStyle
  })
  .render(({ className, style, value }) => (
    <input
      onClick={e => (e.target as any).select()}
      readOnly
      style={style}
      className={className}
      value={value}
    />
  ))
